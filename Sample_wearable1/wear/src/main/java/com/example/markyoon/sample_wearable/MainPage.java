package com.example.markyoon.sample_wearable;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.Timer;
import java.util.TimerTask;

public class MainPage extends LinearLayout implements View.OnClickListener {
    Context mContext;
    private TimerTask second;
    private ImageView walking_1, walking_2, normal, clean_1, clean_2, feed_1, feed_2, sleep_1, sleep_2;
    private final Handler handler = new Handler();
    public int timer_sec = 0;
    private Button map_button;
 //   private static int anim_case = 0;

    public static final int CALL_NUMBER = 1001;

    public MainPage(Context context) {
        super(context);

        init(context);
    }

    public MainPage(Context context, AttributeSet attrs) {
        super(context, attrs);

        init(context);
    }

    private void init(Context context) {
        mContext = context;

        // inflate XML layout
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.mainpage, this, true);
        walking_1 = (ImageView) findViewById(R.id.walking_1);
        walking_2 = (ImageView) findViewById(R.id.walking_2);
        normal = (ImageView) findViewById(R.id.normal);
        clean_1 = (ImageView) findViewById(R.id.clean_1);
        clean_2 = (ImageView) findViewById(R.id.clean_2);
        feed_1 = (ImageView) findViewById(R.id.feed_1);
        feed_2 = (ImageView) findViewById(R.id.feed_2);
        sleep_1 = (ImageView) findViewById(R.id.sleep_1);
        sleep_2 = (ImageView) findViewById(R.id.sleep_2);

        //  testStart();


    }

    public void onClick(View view) {


    }

    public void testStart(final int anim_case) {

        normal.setVisibility(INVISIBLE);
        walking_1.setVisibility(INVISIBLE);
        walking_2.setVisibility(INVISIBLE);
        clean_1.setVisibility(INVISIBLE);
        clean_2.setVisibility(INVISIBLE);
        feed_1.setVisibility(INVISIBLE);
        feed_2.setVisibility(INVISIBLE);
        sleep_1.setVisibility(INVISIBLE);
        sleep_2.setVisibility(INVISIBLE);

        second = new TimerTask() {
            @Override
            public void run() {
                Log.i("Test", "Timer start");
                if(timer_sec==5){
                    timer_sec=0;
                    
                    this.cancel();
                }
                if (timer_sec % 2 == 0) Update(anim_case);
                else Update2(anim_case);
                timer_sec++;
            }
        };
        Timer timer = new Timer();
        timer.schedule(second, 0, 1000);
    }

    protected void Update(final int anim_case) {
        Runnable updater = new Runnable() {
            public void run() {
                if(anim_case==0){
                    sleep_1.setVisibility(View.INVISIBLE);
                    sleep_2.setVisibility(View.VISIBLE);
                }
                else if(anim_case==1){
                    feed_1.setVisibility(View.INVISIBLE);
                    feed_2.setVisibility(View.VISIBLE);
                }
                else if(anim_case==2){
                    clean_1.setVisibility(View.INVISIBLE);
                    clean_2.setVisibility(View.VISIBLE);
                }
                else if(anim_case==3){
                    walking_1.setVisibility(View.INVISIBLE);
                    walking_2.setVisibility(View.VISIBLE);
                }
            }
        };
        handler.post(updater);
    }

    protected void Update2(final int anim_case) {
        Runnable updater = new Runnable() {
            public void run() {
                if(anim_case==0){
                    sleep_2.setVisibility(View.INVISIBLE);
                    sleep_1.setVisibility(View.VISIBLE);
                }
                else if(anim_case==1){
                    feed_2.setVisibility(View.INVISIBLE);
                    feed_1.setVisibility(View.VISIBLE);
                }
                else if(anim_case==2){
                    clean_2.setVisibility(View.INVISIBLE);
                    clean_1.setVisibility(View.VISIBLE);
                }
                else if(anim_case==3){
                    walking_2.setVisibility(View.INVISIBLE);
                    walking_1.setVisibility(View.VISIBLE);
                }
            }
        };
        handler.post(updater);
    }

/*
    public void testStart2() {
        clean_1 = (ImageView) findViewById(R.id.clean_1);
        clean_2 = (ImageView) findViewById(R.id.clean_2);
        normal = (ImageView) findViewById(R.id.normal);
        second = new TimerTask() {
            @Override
            public void run() {
                Log.i("Test", "Timer start");
                {
                    normal.setVisibility(VISIBLE);
                    clean_1.setVisibility(INVISIBLE);
                    clean_2.setVisibility(INVISIBLE);

                }
                if (timer_sec % 2 == 0) Update3();
                else Update4();
            }
        };
        Timer timer = new Timer();
        timer.schedule(second, 0, 1000);
    }
    protected void Update3() {
        Runnable updater = new Runnable() {
            public void run() {
                clean_1.setVisibility(View.INVISIBLE);
                clean_2.setVisibility(View.VISIBLE);
            }
        };
        handler.post(updater);
    }

    protected void Update4() {
        Runnable updater = new Runnable() {
            public void run() {
                clean_1.setVisibility(View.VISIBLE);
                clean_2.setVisibility(View.INVISIBLE);
            }
        };
        handler.post(updater);
    }
*/
}

