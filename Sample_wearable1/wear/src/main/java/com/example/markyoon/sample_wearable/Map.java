package com.example.markyoon.sample_wearable;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.InputStream;

public class Map extends Activity implements View.OnClickListener {

    protected static final String STATIC_MAP_API_ENDPOINT = "http://maps.googleapis.com/maps/api/staticmap?center=37.4223662,-122.0839445&zoom=15&size=200x200&key=AIzaSyBswt3M3BmqydcfJt0qY22RMp25G9juHAM";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_1);
        

        AsyncTask<Void, Void, Bitmap> setImageFromUrl = new AsyncTask<Void, Void, Bitmap>(){
            @Override
            protected Bitmap doInBackground(Void... params) {
                Bitmap bmp = null;
                HttpClient httpclient = new DefaultHttpClient();
                HttpGet request = new HttpGet(STATIC_MAP_API_ENDPOINT);

                InputStream in = null;
                try {
                    in = httpclient.execute(request)
                            .getEntity()
                            .getContent();
                    bmp = BitmapFactory.decodeStream(in);
                    in.close();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
                return bmp;
            }


            protected void onPostExecute(Bitmap bmp) {
                if (bmp!=null) {
                    final ImageView iv = (ImageView) findViewById(R.id.img);
                    iv.setImageBitmap(bmp);
                }
            }
        };
        setImageFromUrl.execute();




    }

    @Override
    public void onClick(View v) {


    }
}